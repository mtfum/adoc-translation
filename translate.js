'use strict'
const fs = require('fs')
const config = require('./config')

let langList = config.LANG_LIST
let fileList = config.FILE_LIST
let languageCategory = config.LANGUAGE_CATEGORY
let translationSheetJSON =  JSON.parse(fs.readFileSync(config.TRANSLATION_SHEET_JSON, 'utf8'))
let assetsDirectory = config.ASSETS_DIRECTORY

for (var i = 0; i < langList.length; i++) {
  var list = []
  let lang = langList[i]
  for (var j = 0; j < fileList.length; j++) {

    var inputFileFullPath = assetsDirectory + `${fileList[j]}_${lang}.plist`
    // pathからxmlを作成
    var xml = fs.readFileSync(inputFileFullPath, "utf-8")

    if (list.length === 0) {
      for (var k = 0; k < translationSheetJSON.length; k++) {
       let sheet = translationSheetJSON[k]
       list.push({
         before: sheet['日本語'],
         after: sheet[languageCategory[lang][0]]
       })
       list.push({
         before: sheet.field2,
         after: sheet[languageCategory[lang][1]]
       })
       list.push({
         before: sheet.field3,
         after: sheet[languageCategory[lang][2]]
       })
      }
    }
    for (var l = 0; l < list.length; l++) {
      let item = list[l]
      let replaceString = new RegExp(`<string>${item.before}</string>`.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&"), 'gm')

      if (item.after != "✓") {
        xml = xml.replace(replaceString, `<string>${item.after}</string>`)
      }
    }
    fs.writeFileSync(inputFileFullPath, xml)
  }
}
