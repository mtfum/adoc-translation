# adoc-translationの使い方

- node.jsの環境設定を行ってください
- `config.js`にある、`TRANSLATION_SHEET_JSON` `ASSETS_DIRECTORY` `FILE_LIST`, `LANGUAGE_CATEGORY`, `LANG_LIST`の設定をします
- ターミナルで`node translate.js`を実行して、plistの確認します

# 別途連絡事項

- infoList29~32: sheet(line: 265)の中国語の翻訳が足りないので要確認
- 水色背景の部分はplistへの整形のために改行したり、cellを横にずらしたりなどしたので確認お願いします。
- 翻訳がプログラムでできていなくて手動で行った部分(目視で確認できる範囲)
  1. imgList.plist line: 521 `地域活動(PTAなど)への参加`(sheet line: 152)
