module.exports = {
  // jsonの用意, 下記が楽でした。
  //  1. $ npm install csvtojson
  //  2. `$ csvtojson ./ADOC-MessageChinese-last\ -\ Sheet1.csv > converted.json`
  // 用意したJSONファイルパスを`./converted.json`に設定
  TRANSLATION_SHEET_JSON: './converted.json',

  // Xcodeプロジェクト内のAssetsファイルのディレクトリを指定してください
  ASSETS_DIRECTORY:  "/Users/hoge/hoge/Developments/mitolab/adoc/Resources/Assets/",

  // 翻訳するファイル接頭辞リスト
  // 追加、削除をして下さい
  FILE_LIST: ["ageList", "etc", "imgList", "infoList", "diagnosisList", "sexList", "sheetStrings"],

  // 翻訳言語 接尾語リスト
  // 言語が増えたらlanguageCategoryも合わせて追加してください
  LANG_LIST: ["ch-si", "ch-tr"],

  // 翻訳言語とExcel(converted.json)の対応列
  LANGUAGE_CATEGORY: {
    'ja'    : ['日本語', "field2", "field3"],
    "ch-si" : ["中国語（簡体字）", "field8", "field9"],
    "ch-tr" : ["中国語（繁体字）", "field5", "field6"]
  }
}
